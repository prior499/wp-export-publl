<?php
require_once '../../../../wp-load.php';

define( 'PExp_URL', "http://localhost/script.php" );

function processingString($string) {
    $string = trim($string);
    $string = strip_tags($string);
    $string = htmlspecialchars($string);
    return $string;
}

if (is_user_logged_in() && !empty( $_POST['api_publl'] ) && isset( $_POST['_wpnonce'] ) && wp_verify_nonce( $_POST['_wpnonce'], 'publl_export' )) {
    check_admin_referer('publl_export');

    global $wpdb;
    $api = processingString($_POST['api_publl']);
    $date = processingString($_POST['post_date']);
    $post_status = $_POST['post_status'];
    $date_from = processingString($_POST['post_date_from']);
    $date_to = processingString($_POST['post_date_to']);

    $countarticles = 0;
    $export_status = [];
    $value_parameter = [];

    $query_select = "ID, post_content, post_title, post_status";
    if(!empty($date)) {
        $query_select .= ', post_date';
    }

    $query_post_status = "";
    if(empty($post_status)) {
        $export_status[] = "SelectPStat";
    } else {
        foreach($post_status as $key => $status){
            if($status === "publish" || $status === "draft") {
                $query_post_status .= "'".$status."'";
                if ($status != end( $post_status)) {
                    $query_post_status .= ', ';
                }
            } else {
                $export_status[] = "CheckPStat";
                break;
            }
        }
    }

    $query_where = "post_status IN (".$query_post_status.") AND post_type = 'post'";
    $query .= "SELECT ".$query_select;
    $query .= " FROM ".$wpdb->posts;
    $query .= " WHERE ".$query_where;

//    $query = "SELECT ". $query_select ." FROM ". $wpdb->posts ." WHERE post_status = 'publish' AND post_type = 'post'";

    if ( !empty( $date_from ) && !empty( $date_to ) ) {
        $query .= " AND post_date BETWEEN '%s' AND '%s' ";
        $value_parameter[] = $date_from . ' 00:00:00';
        $value_parameter[] = $date_to . ' 23:59:59';
    } elseif (!empty( $date_from ) && empty( $date_to )) {
        $query .= " AND post_date BETWEEN '%s' AND '%s' ";
        $value_parameter[] = $date_from . ' 00:00:00';
        $value_parameter[] = date("Y-m-d") . ' 23:59:59';
    } elseif (empty( $date_from ) && !empty( $date_to )) {
        $query .= " AND post_date BETWEEN '%s' AND '%s' ";
        $value_parameter[] = '2003-05-27 00:00:00';
        $value_parameter[] = $date_to . ' 23:59:59';
    }

    if(empty($export_status)) {
        $prepare = $wpdb->prepare( $query, $value_parameter );
        $exportdata['Articles'] = $wpdb->get_results( $prepare, ARRAY_A );
        $exportdata['API'] = $api;

        if(!empty($exportdata['Articles'])) {
            $ch = curl_init(PExp_URL);
            $data_string = json_encode($exportdata);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, array("wp_export"=>$data_string));

            $result = curl_exec($ch);
            curl_close($ch);

            $countarticles = "&count-articles=".count($exportdata['Articles']);
            if(!$result) {
                $export_status[] = "503";
            }
        } else {
            $export_status[] = "emArticle";
        }
    }

    if(empty($export_status)) {
        $export_status[] = "OK";
    }

    // Export status code
    // OK - Export articles: OK.
    // SelectPStat - Select export post status.
    // CheckPStat - Check export post status.
    // emArticle - Articles not found.
    // 503 - Server Publl is not responding, try again later.
    // erAPI - API Key does not match. Check and try again.
    // emAPI - API Key not found. Check and try again.
    // bAPI - API Key is banned.

    $status_get = '';
    $countstat = count($export_status);
    for($i = 0; $i < $countstat; $i++){
        $status_get .= "&export-status[]=".$export_status[$i];
    }

    $bits = explode('?',$_SERVER['HTTP_REFERER']);
    $redirect = $bits[0];
    header("Location:".$redirect."?page=".PExp_PLUGIN_NAME.$status_get.$countarticles);

    exit;
}

