<?php
/*
Plugin Name: Publl Export
Version: 0.1
Author: Yury
*/

add_action('admin_menu', function(){
    add_menu_page( 'Export article to Publl', 'Export Publl', 'manage_options', 'publlexport', 'add_my_setting', 'dashicons-migrate', 75 );
//    add_submenu_page( 'tools.php', 'Export article to Publl', 'Export Publl', 'manage_options', 'publlexport', 'add_my_setting' );
} );

define( 'PExp_PLUGIN_BASENAME', plugin_basename( __FILE__ ) );
define( 'PExp_PLUGIN_NAME', trim( dirname( PExp_PLUGIN_BASENAME ), '/' ) );
define( 'PExp_PLUGIN_DIR', untrailingslashit( dirname( __FILE__ ) ) );
define( 'PExp_PLUGIN_URL', untrailingslashit( plugins_url( '', __FILE__ ) ) );

function add_my_setting(){
    ?>
    <div class="wrap">
        <h2><?php echo get_admin_page_title() ?></h2>
        <form action="<?php echo PExp_PLUGIN_URL .'/admin/export.php'; ?>" method="POST">
            <div class="setting">
                <h3></h3>
                <ul class="setting_list">
                    <li><label>API Key Publl: <input type="text" name="api_publl" required></label></li>
                    <li><label><input type="checkbox" name="post_name" checked="checked" disabled>Title*</label></li>
                    <li><label><input type="checkbox" name="post_author" checked="checked" disabled>Content*</label></li>
                    <li><label><input type="checkbox" name="post_date">Date</label></li>
                    <li>Select post status:
                        <ul>
                            <li style="display: inline-block"><label><input type="checkbox" name="post_status[]" value="publish" class="post_status" checked="checked">Publish</label></li>
                            <li style="display: inline-block"><label><input type="checkbox" name="post_status[]" value="draft" class="post_status">Draft</label></li>
                        </ul>
                    </li>
                    <tr>
                        <th>Select date: </th>
                        <td id="post_date">
                            <label for="post_date">from</label>
                            <input type="date" name="post_date_from" class="post_date-datepicker" placeholder="yyyy-mm-dd" />
                            <label for="post_date-datepicker-to">to</label>
                            <input type="date" name="post_date_to" class="post_date-datepicker" placeholder="yyyy-mm-dd" />
                        </td>
                    </tr>

                </ul>
            </div>
            <?php
            wp_nonce_field('publl_export'); //
            submit_button('Export', 'action', '', false, array( 'action' => "export" ) );
            ?>
        </form>
        <span class="export-status">
        <?php
        if(isset($_GET['export-status'])) {
//            echo "<script>setTimeout(\"document.getElementsByClassName('export-status')[0].innerHTML = '';\", 15000);</script>";
            foreach ($_GET['export-status'] as $val) {
                switch ($val) {
                    case "OK":
                        echo "<h2 style='color: green'>Export ".$_GET['count-articles']." articles.</h2>";
                        break;
                    case "SelectPStat":
                        echo "<h2 style='color: orange'>Select export post status.</h2>";
                        break;
                    case "CheckPStat":
                        echo "<h2 style='color: orange'>Check export post status.</h2>";
                        break;
                    case "emArticle":
                        echo "<h2 style='color: orange'>Articles not found.</h2>";
                        break;
                    case "503":
                        echo "<h2 style='color: red'>Server Publl is not responding, try again later.</h2>";
                        break;
                    case "erAPI":
                        echo "<h2 style='color: red'>API Key does not match. Check and try again.</h2>";
                        break;
                    case "emAPI":
                        echo "<h2 style='color: orange'>API Key not found. Check and try again.</h2>";
                        break;
                    case "bAPI":
                        echo "<h2 style='color: red'>API Key is banned.</h2>";
                        break;
                    default:
                        echo "<h2 style='color: orange'>Unknown error.</h2>";
                }
            }
        }
        ?>
        </span>
    </div>
<?php
}
